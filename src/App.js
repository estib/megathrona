import './App.css';
import React from "react";

const Option = (props) => {
  return (
    <div className='option'>
      <input type="radio" id={props.id} name={props.name} />
      <label htmlFor={props.id} >{props.label}</label>
    </div>
  )
}

const Question = (props) => {
  return (
    <div className='question'>
      <h4>{props.question}</h4>
      {
        props.options.map((o, i) => <Option key={i} id={o.id} label={o.label} name={props.question} />)
      }
    </div>
  );
}

const questions = [
  {
    question: "Is your relationship status:",
    options: [
      { id: "option-a", label: "Married" },
      { id: "option-b", label: "Divorced" },
      { id: "option-c", label: "Single and ready to mingle" },
    ]
  },
  {
    question: "If you were DJ in a road-trip, which music genre would you play?",
    options: [
      { id: "option-a", label: "70s, 80s, 90s" },
      { id: "option-b", label: "R&B" },
      { id: "option-c", label: "Pop" },
      { id: "option-d", label: "A combination of K-Pop and Reggaeton, with some songs here and there by Phineas & Ferb (in Italian)" },
    ]
  },
  {
    question: "How gemini are you?",
    options: [
      { id: "option-a", label: "You're like super nice" },
      { id: "option-b", label: "You have no flaws like at all" },
      { id: "option-c", label: "You laugh at you're own jokes because you're hilarious" },
      { id: "option-d", label: "All of the above" },
    ]
  },
  {
    question: "If you were lactose intolerant would you...",
    options: [
      { id: "option-a", label: "Become vegan" },
      { id: "option-b", label: "Avoid lactose-heavy foods" },
      { id: "option-c", label: "Make an exception for gelato ONLY" },
      { id: "option-d", label: "Eat a fucking stracciatella pizza because it's delicious and its worth it" },
    ]
  },
  {
    question: "When meeting the sister of your romantic interest, would you...",
    options: [
      { id: "option-a", label: "Hug her" },
      { id: "option-b", label: "Do a peace sign ✌️" },
      { id: "option-c", label: "Stop the incoming hug. Shake her hand and let your best friend hug her instead" },
    ]
  },
  {
    question: "What is your opinion about Spider-Man?",
    options: [
      { id: "option-a", label: "LOOOOOOOVE IT" },
      { id: "option-c", label: "Best. Thing. Ever." },
      { id: "option-d", label: "Mmmm next question please" },
    ]
  },
  {
    question: "If given the girlfriend-position, would you offer...",
    options: [
      { id: "option-a", label: "Big, beautiful brown eyes (sometimes smaller due to glasses)" },
      { id: "option-b", label: "Long dark hair (that always is super nice)" },
      { id: "option-c", label: "A nose that knows no imperfection" },
      { id: "option-d", label: "A great sense of humor" },
      { id: "option-e", label: "A super cute laugh" },
      { id: "option-f", label: "Slow-motion twerking (in progress of becoming faster)" },
      { id: "option-g", label: "All of the above" },
    ]
  },
];

const ConsideringScreen = props => {
  if (props.hide) {
    return <React.Fragment />;
  }
  return (
    <div className='loading-screen'>
      <div className='title'>
        <h1>{props.label}</h1>
      </div>
    </div>
  )
};

const FinalScreen = props => {
  if (props.hide) {
    return <React.Fragment />;
  }
  return (
    <div className='final-screen'>
      <div className='title'>
        <h1>Call me ❤️</h1>
      </div>
    </div>
  )
};

const messages = [
  "Processing",
  "Still processing...",
  "Just a sec...",
  "Maybe you should think about it",
  "What do you think? 🤞",
  "You know what? I think I know what I want",
  "I hope you agree with my decision 😍",
  "❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️❤️",
]

function App() {
  const [disclaimer1, setDisclaimer1] = React.useState(false);
  const [disclaimer2, setDisclaimer2] = React.useState(false);
  const [showScreen, setShowScreen] = React.useState(false);
  const [showFinalScreen, setShowFinalScreen] = React.useState(false);


  let messageIndex = 0;
  const [message, setMessage] = React.useState(messages[messageIndex]);

  const updateMessage = () => {
    setTimeout(() => {
      if (messageIndex < (messages.length - 1)) {
        messageIndex += 1;
        setMessage(messages[messageIndex]);
        updateMessage();
      } else {
        setShowScreen(false);
        setShowFinalScreen(true);
      }
    }, 4000);
  }

  const handleSubmit = () => {
    setShowScreen(true);
    updateMessage();
  };

  return (
    <div className="App">
      <h3>
        Interview for Crush / Girlfriend position
      </h3>
      <p>
        Please answer these questions honestly.
      </p>
      <div className="count-down-container">
        {
          questions.map((q, i) => <Question key={i} {...q} />)
        }
        <div className='disclaimer'>
          <input id="dis" type="checkbox" value={disclaimer1} onChange={e => setDisclaimer1(e.target.checked)} />
          <label htmlFor="dis">I'm not a robot, unless you're Megathrona 🤖</label>
        </div>
        <div className='disclaimer'>
          <input id="dis" type="checkbox" value={disclaimer2} onChange={e => setDisclaimer2(e.target.checked)} />
          <label htmlFor="dis">I hereby declare that all my answers were truthful and that I found this kind of funny and maybe cute. Still I appreciate it ❤️</label>
        </div>
        <div
          className={disclaimer1 === true && disclaimer2 === true ? 'button' : 'button-disabled'}
          onClick={handleSubmit}
        >
          Submit
        </div>
      </div>
      <ConsideringScreen hide={!showScreen} label={message} />
      <FinalScreen hide={!showFinalScreen} />
    </div>
  );
}

export default App;
